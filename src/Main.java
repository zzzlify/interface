class Figure {
    private String mColor;

    public Figure(String color) {
        mColor = color;
    }

    public String getColor() {
        return mColor;
    }
}
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
class Quadrate extends Figure{
    private int mStorona;
    public Quadrate(String color,int storona) {
        super(color);
        mStorona = storona;
    }
    public void getSquare() {
        System.out.println("Square:"+mStorona*mStorona);
    }
}
/////////////////////////////////////////////////////////////////
class Rectangle extends Figure{
    private int mStorona1;
    private int mStorona2;
    public Rectangle(String color,int storona1,int storona2) {
        super(color);
        mStorona1 = storona1;
        mStorona2 = storona2;
    }
    public void getSquare() {
        System.out.println("Square:"+mStorona1*mStorona2);
    }
}
/////////////////////////////////////////////////////////////////
class Circle extends Figure{
    public final double pi = 3.14159265359;
    private int mRadius;
    public Circle(String color,int radius) {
        super(color);
        mRadius = radius;
    }
    public void getSquare() {
        System.out.println("Square:"+mRadius*mRadius*pi);
    }
}


public class Main {

    public static void main(String[] args) {
        Quadrate kv1 = new Quadrate("Red",5);
        System.out.println(kv1.getClass());
        System.out.println("Color:"+kv1.getColor());
        kv1.getSquare();

        System.out.println();

        Rectangle re1 = new Rectangle("Blue",5,2);
        System.out.println(re1.getClass());
        System.out.println("Color:"+re1.getColor());
        re1.getSquare();

        System.out.println();

        Circle ci1 = new Circle("Yellow",4);
        System.out.println(ci1.getClass());
        System.out.println("Color:"+ci1.getColor());
        ci1.getSquare();
    }
}



